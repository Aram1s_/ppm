fn main() {
    let data = std::fs::read("images/raw_2.ppm").unwrap();
    let sys_time = std::time::SystemTime::now();
    let ppm = ppm::parse(&data).expect("failed to parse data");
    println!(
        "time elapsed from parsing: {:?}",
        sys_time.elapsed().unwrap()
    );
    println!("max val: {}", ppm.max_val);
    println!("first pixel: {:?}", ppm.pixels[0][0]);
    println!("image height: {}", ppm.pixels.len());
    println!("image width: {}", ppm.pixels[0].len());
    std::fs::write("en.ppm", ppm::encode_raw(ppm)).unwrap();

    // let ppm = ppm::parse_plain_file("test_images/ascii.ppm").expect("failed to parse data");
    // println!("max val: {}", ppm.max_val);
    // println!("first pixel: {:?}", ppm.pixels[0][0]);
}
