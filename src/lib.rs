pub struct PPM {
    pub max_val: u32,
    pub pixels: Vec<Vec<Pixel>>,
}

#[derive(Clone, Debug)]
pub struct Pixel {
    pub r: u16,
    pub g: u16,
    pub b: u16,
}

/// Returns Error containing byte index
#[derive(Debug)]
pub enum Errors {
    Utf8Error,
    ParseError,
}

/// A "magic number" for identifying the file type. A ppm image's magic number is the two characters "P6".
/// Whitespace (blanks, TABs, CRs, LFs).
/// A width, formatted as ASCII characters in decimal.
/// Whitespace.
/// A height, again in ASCII decimal.
/// Whitespace.
/// The maximum color value (Maxval), again in ASCII decimal. Must be less than 65536 and more than zero.
/// A single whitespace character (usually a newline).
/// A raster of Height rows, in order from top to bottom. Each row consists of Width pixels, in order from left to right. Each pixel is a triplet of red, green, and blue samples, in that order. Each sample is represented in pure binary by either 1 or 2 bytes. If the Maxval is less than 256, it is 1 byte. Otherwise, it is 2 bytes. The most significant byte is first.
pub fn parse(data: &Vec<u8>) -> Result<PPM, Errors> {
    assert!(data[..2] == *b"P6");
    let mut pointer = 3;
    if let Ok("#") = std::str::from_utf8(&data[pointer..pointer + 1]) {
        while let Ok(c) = std::str::from_utf8(&data[pointer..pointer + 1]) {
            pointer += 1;
            if c == "\n" {
                break;
            }
        }
    }

    let height = std::str::from_utf8(&item(data, &mut pointer))
        .map_err(|_| Errors::Utf8Error)?
        .parse()
        .map_err(|_| Errors::ParseError)?;

    let width = std::str::from_utf8(&item(data, &mut pointer))
        .map_err(|_| Errors::Utf8Error)?
        .parse()
        .map_err(|_| Errors::ParseError)?;

    let max_val: u32 = std::str::from_utf8(&item(data, &mut pointer))
        .map_err(|_| Errors::Utf8Error)?
        .parse()
        .map_err(|_| Errors::ParseError)?;

    assert!(max_val < 65536);

    let sample_size = if max_val < 256 { 1 } else { 2 };

    let data = &data[pointer..];

    assert_eq!(data.len() % (sample_size * 3), 0);

    let mut pixels: Vec<Vec<Pixel>> = vec![vec![Pixel { r: 0, g: 0, b: 0 }; width]; height];

    parse_pixels(&data, sample_size, &mut pixels);

    Ok(PPM { max_val, pixels })
}

fn parse_pixels(data: &[u8], sample_size: usize, pixels: &mut [Vec<Pixel>]) -> Option<()> {
    let mut data = data.iter();
    for i in 0..pixels.len() {
        for j in 0..pixels[0].len() {
            if sample_size == 1 {
                pixels[i][j] = Pixel {
                    r: (*data.next()?).into(),
                    g: (*data.next()?).into(),
                    b: (*data.next()?).into(),
                };
            } else {
                pixels[i][j] = Pixel {
                    r: (*data.next()?) as u16 | (((*data.next()?) as u16) << 8),
                    g: (*data.next()?) as u16 | (((*data.next()?) as u16) << 8),
                    b: (*data.next()?) as u16 | (((*data.next()?) as u16) << 8),
                };
            }
        }
    }
    Some(())
}

pub fn parse_plain_file(file_name: &str) -> Result<PPM, Errors> {
    use std::fs::File;
    use std::io::prelude::*;
    use std::io::BufReader;

    let f = File::open(file_name).unwrap();
    let mut reader = BufReader::new(f);
    let mut data = String::new();

    reader.read_line(&mut data).unwrap();
    while &data[0..1] == "#" {
        data.clear();
        reader.read_line(&mut data).unwrap();
    }
    assert!(data.trim() == "P3");

    data.clear();
    reader.read_line(&mut data).unwrap();
    while &data[0..1] == "#" {
        data.clear();
        reader.read_line(&mut data).unwrap();
    }

    let mut d = data.split_whitespace();
    let height = d.next().unwrap().parse().unwrap();
    let width = d.next().unwrap().parse().unwrap();

    data.clear();
    reader.read_line(&mut data).unwrap();
    let max_val = data.trim().parse().map_err(|_| Errors::ParseError)?;

    assert!(max_val < 65536);
    data.clear();

    let mut pixels: Vec<Vec<Pixel>> = vec![vec![Pixel { r: 0, b: 0, g: 0 }; width]; height];
    for i in 0..pixels.len() {
        for j in 0..pixels[0].len() {
            let mut pixel = Pixel { r: 0, g: 0, b: 0 };

            data.clear();
            reader.read_line(&mut data).unwrap();
            pixel.r = data.trim().parse().unwrap();

            data.clear();
            reader.read_line(&mut data).unwrap();
            pixel.g = data.trim().parse().unwrap();

            data.clear();
            reader.read_line(&mut data).unwrap();
            pixel.b = data.trim().parse().unwrap();

            pixels[i][j] = pixel;
        }
    }

    Ok(PPM { max_val, pixels })
}

fn item(data: &[u8], pointer: &mut usize) -> Vec<u8> {
    let mut item = Vec::new();
    for b in &data[*pointer..] {
        match b {
            b' ' | b'\n' | b'\t' => {
                *pointer += item.len() + 1;
                return item;
            }
            _ => item.push(*b),
        }
    }
    data.to_vec()
}

pub fn encode_raw(ppm: PPM) -> Vec<u8> {
    let mut bytes: Vec<u8> = format!(
        "P6\n{} {}\n{}\n",
        ppm.pixels.len(),
        ppm.pixels[0].len(),
        ppm.max_val
    )
    .as_bytes()
    .to_vec();
    let sample_size = if ppm.max_val < 256 { 1 } else { 2 };
    for pixel in ppm.pixels.iter().flatten() {
        if sample_size == 1 {
            bytes.push(pixel.r as u8);
            bytes.push(pixel.g as u8);
            bytes.push(pixel.b as u8);
        } else {
            let a = [
                pixel.r as u8,
                (pixel.r >> 8) as u8,
                pixel.g as u8,
                (pixel.g >> 8) as u8,
                pixel.b as u8,
                (pixel.b >> 8) as u8,
            ];
            a.iter().for_each(|m| bytes.push(*m));
        }
    }
    bytes
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn raw_decode() {
        parse(&std::fs::read("test_images/raw.ppm").unwrap()).unwrap();
    }

    #[test]
    fn plain_decode() {
        parse_plain_file("test_images/ascii.ppm").unwrap();
    }
}
